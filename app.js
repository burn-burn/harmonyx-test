const express = require('express')
const app = express()
const port = 3000

app.use(express.json())

app.get('/indices-numbers', (req, res) => {
  res.send(indicesNumbers( req.query.nums.map((num) => parseInt(num)), parseInt(req.query.target), 0))
})

app.get('/change-money', (req, res) => {
  let banknotes = [1000, 500, 100, 50, 20, 10, 5, 2, 1] 
  let changeAmount =  parseInt(req.query.receivedAmount) - parseInt(req.query.price)
  let change = []
  for (banknote of banknotes) {
    change.push({
      value: banknote,
      amount: banknote <= changeAmount ? Math.floor( changeAmount / banknote ): 0
    })
    changeAmount = changeAmount % banknote
  }
  res.send(change)
})


app.listen(port, () => {
  console.log(`listening on port ${port}`)
})

function indicesNumbers(numbers, target, point) {
  let index = numbers.indexOf(target - numbers[point])
  if(index == -1 || index == point) return indicesNumbers(numbers, target, point + 1)
  return [point, index]
}
