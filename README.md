# ส่งแบบทดสอบ

เลือกทำข้อที่ 1 และข้อที่ 2 
ทั้ง 2 ข้อเขียนอยู่บน nodeJs + express แยกเป็น endpoint 2 ตัวมีรายละเอียดดังนี้ครับ


## ข้อที่ 1

**[Get] /indices-numbers**

**Query params**   
	- nums (Array of number)
	- target (Number)


**CURL Example**
```curl
curl --location -g --request GET 'http://localhost:3000/indices-numbers?nums[0]=2&nums[1]=5&nums[2]=11&nums[3]=7&target=12'
```

## ข้อที่ 2

**[Get] /change-money**

**Query params**   
	- price (Number)
	- receivedAmount (Number)


**CURL Example**
```curl
curl --location --request GET 'http://localhost:3000/change-money?price=35&receivedAmount=100'
```

